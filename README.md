Universidade de Brasília - 2017/1º semestre - Faculdade do Gama
Aluno: João Pedro de Aquino Corrêa Martins
Matrícula: 16/0046602

Orientação a Objetos - Professor Renato Coral

Exercício de Programação 1

Este exercício busca avaliar os seguintes conceitos: 

  • Criação de classes e objetos.
  • Utilização de atributos, métodos e construtores.
  • Utilização de herança.

Para efeitos de compilação do código, deve-se importar o projeto para o seu computador. Em seguida,
vá ao terminal de execução e direcione-se a pasta do projeto. Digite "make clean", depois digite "make" e, por fim, digite "make run" para a execução do código. Para os usuários de Windows, vá a pasta "bin" e execute o código clicando no ícone com o nome "saida". 


