#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "GameObject.hpp"
#include <iostream>
#include <string>

using namespace std;
//Neste jogo, o jogador será representado por "W"
class Player : public GameObject
{
  private:
    //Atributos
    int score, life;
    bool winner;
  public:
    //Métodos
    Player();
    Player(char sprite, int positionX, int positionY, int life, int score);
    int getScore();
    void setScore(int score);
    int getLife();
    void setLife(int life);
};
#endif
