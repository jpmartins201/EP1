#ifndef COLLIDER_H_DEFINED
#define COLLIDER_H_DEFINED
#include "GameObject.hpp"
#include "Player.hpp"
#include "Trap.hpp"
#include "Bonus.hpp"
#include <Map.hpp>
#include <ncurses.h>
#include <iostream>
#include <string>
#include <stdio_ext.h>
#include <stdlib.h>
#include <unistd.h>

using namespace std;

class Collider // Class for all collisions in the game
{
public:
  Collider();
  void wallCollider(Player * player, Map * map, char direction);
  void trapCollider(Player * player, Trap * trap, char direction);
  void bonusCollider(Player * player, Bonus * bonus_score, char direction);
};
#endif
