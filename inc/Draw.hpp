#ifndef DRAW_HPP
#define DRAW_HPP
#include "Map.hpp"
#include "GameObject.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdlib.h>

using namespace std;

class Draw
{
  public:
    Draw();
    void DrawMap();
};
#endif
