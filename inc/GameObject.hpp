#ifndef GAMEOBJECT_H_DEFINED
#define GAMEOBJECT_H_DEFINED

#include <iostream>
#include <string>

using namespace std;

class GameObject
{
  protected:
    //Atributos
    int positionX, positionY; //Posição x e y na matriz "mapa"
    char sprite; //Caracterização de qualquer objeto no jogo (player, trap e bonus)
  public:
    //Métodos
    GameObject();
    GameObject(char sprite, int positionX, int positionY);
    int getPositionX();
    void setPositionX(int positionX);
    int getPositionY();
    void setPositionY(int positionY);
    char getSprite();
    void setSprite(char sprite);
    //bool Colisao(char sprite, int positionX, int positionY);
};
#endif
