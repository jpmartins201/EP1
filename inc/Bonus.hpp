#ifndef BONUS_HPP
#define BONUS_HPP

#include "GameObject.hpp"
#include <iostream>
#include <string>

using namespace std;
//Neste jogo, bônus será representado por "B"
class Bonus: public GameObject
{
  private:
    int bonus_score;
  public:
    Bonus();
    Bonus(char sprite, int positionX, int positionY, int bonus_score);
    int getBonus();
    void setBonus(int bonus_score);
};
#endif
