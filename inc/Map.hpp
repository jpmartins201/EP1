#ifndef MAP_H_DEFINED
#define MAP_H_DEFINED
#define ALTURA 20
#define COMPRIMENTO 50
#include "GameObject.hpp"
#include <string>
#include <iostream>

using namespace std;

class Map : public GameObject //class for adding player, traps and bonuses in the window;
{
private:
  char map[ALTURA][COMPRIMENTO];
  char auxiliar[ALTURA][COMPRIMENTO];
public:
  Map();
  void getMap();
  void setMap();
  void addSprite(char sprite, int positionX, int positionY);
  char Detector(int positionX, int positionY);
};
#endif
