#ifndef TRAP_HPP
#define TRAP_HPP

#include "GameObject.hpp"
#include <iostream>
#include <string>

using namespace std;
//Traps will be represented as "@"
class Trap : public GameObject
{
  private:
  //Attributes
    int damage;
  public:
  //Methods
    Trap();
    Trap(char sprite, int positionX, int positionY, int damage);
    int getDamage();
    void setDamage(int damage);
};
#endif
