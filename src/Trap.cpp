#include "Trap.hpp"
#include "GameObject.hpp"
#include <string>
#include <fstream>
#include <ncurses.h>

using namespace std;

Trap::Trap(){}

Trap::Trap(char sprite, int positionX, int positionY, int damage){
  this->sprite = sprite;
  this->positionX = positionX;
  this->positionY = positionY;
  this->damage = damage;
}
void Trap::setDamage(int damage){
  this->damage = damage;
}
int Trap::getDamage(){
  return damage;
}
