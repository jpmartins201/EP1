#include "GameObject.hpp"
#include "Player.hpp"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <ncurses.h>
#include <string>

using namespace std;

Player::Player(){}

Player::Player(char sprite, int positionX, int positionY, int life, int score){
  this->sprite = sprite;
  this->positionX = positionX;
  this->positionY = positionY;
  this->life = life;
  this->score = score;
}
int Player::getLife(){
  return this->life;
}
void Player::setLife(int life){
  this->life -= life;
}
int Player::getScore(){
  return this->score;
}
void Player::setScore(int score){
  this->score += score;
}
