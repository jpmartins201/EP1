#include "Bonus.hpp"
#include <iostream>
#include <string>

using namespace std;

Bonus::Bonus(){}

Bonus::Bonus(char sprite, int positionX, int positionY, int bonus_score){
  this->sprite = sprite;
  this->positionX = positionX;
  this->positionY = positionY;
  this->bonus_score = bonus_score;
}
int Bonus::getBonus(){
  return this->bonus_score;
}
void Bonus::setBonus(int bonus_score){
  this->bonus_score = bonus_score;
}
