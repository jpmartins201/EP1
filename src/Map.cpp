#include "Map.hpp"
#include "GameObject.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio_ext.h>
#include <ncurses.h>

using namespace std;

Map::Map(){}


void Map::setMap()
{
  //Variáveis
  int u, i;
  string aux;
  ifstream map_arq ("Mapa.txt");
  //Guardando os caracteres ASCII do arquivo .txt na memória
  for(i = 0; i < 20; i++){
  	getline(map_arq, aux);
  	for(u = 0; u < 50; u++){
  		this->map[i][u] = aux[u];
      this->auxiliar[i][u] = aux[u];
  	}
  }
  map_arq.close();
}
void Map::getMap(){

  for(int i = 0; i < 20; i++)
  {
    for(int u = 0; u < 50; u++)
    {
      printw("%c", this->map[i][u]);//Printando o mapa gráfico na tela
      this->map[i][u] = this->auxiliar[i][u];
      if(u >= 49){
        printw("\n");
      }
    }
  }
}

char Map::Detector(int positionX, int positionY)
{
  return this->map[positionX][positionY];
}

void Map::addSprite(char sprite, int positionX, int positionY)
{
  this->map[positionY][positionX] = sprite;
}
