#include "GameObject.hpp"
#include "Player.hpp"
#include "Map.hpp"
#include "Collider.hpp"
#include "Bonus.hpp"
#include "Trap.hpp"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <ncurses.h>
#include <string>

using namespace std;

Collider::Collider(){}

void Collider::wallCollider(Player * player, Map * map, char direction)
{
  if(direction == 'w' && map->Detector(player->getPositionY()-1, player->getPositionX()) != '='){
    player->setPositionY(-1);
  }else if(direction == 's' && map->Detector(player->getPositionY()+1, player->getPositionX()) != '='){
      player->setPositionY(1);
    }else if(direction == 'd' && map->Detector(player->getPositionY(), player->getPositionX()+1) != '='){
        player->setPositionX(1);
      }else if(direction == 'a' && map->Detector(player->getPositionY(), player->getPositionX()-1) != '='){
          player->setPositionX(-1);
        }
}

void Collider::bonusCollider(Player *player, Bonus * bonus_score, char direction)
{

  if(direction == 'w' && (player->getPositionY() == bonus_score->getPositionY()) && (player->getPositionX() == bonus_score->getPositionX()))
  {
    player->setScore(+1);
  }
  else
  {
    if(direction == 's' && (player->getPositionY() == bonus_score->getPositionY()) && (player->getPositionX() == bonus_score->getPositionX()))
    {
      player->setScore(+1);
    }
    else
    {
      if(direction == 'd' && (player->getPositionY() == bonus_score->getPositionY()) && (player->getPositionX() == bonus_score->getPositionX()))
      {
        player->setScore(+1);
      }
      else
        if(direction == 'a' && (player->getPositionY() == bonus_score->getPositionY()) && (player->getPositionX() == bonus_score->getPositionX()))
        {
          player->setScore(+1);
        }
    }
  }
}

void Collider::trapCollider(Player * player, Trap * trap, char direction)
{
  if(direction == 'w' && (player->getPositionY() == trap->getPositionY()) && (player->getPositionX() == trap->getPositionX()))
  {
    player->setLife(1);
  }
  else
  {
    if(direction == 's' && (player->getPositionY() == trap->getPositionY()) && (player->getPositionX() == trap->getPositionX()))
    {
      player->setLife(1);
    }
    else
    {
      if(direction == 'd' && (player->getPositionY() == trap->getPositionY()) && (player->getPositionX() == trap->getPositionX()))
      {
        player->setLife(1);
      }
      else
        if(direction == 'a' && (player->getPositionY() == trap->getPositionY()) && (player->getPositionX() == trap->getPositionX()))
        {
          player->setLife(1);
        }
    }
  }
}
