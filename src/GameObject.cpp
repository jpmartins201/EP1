#include "GameObject.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdlib.h>

using namespace std;
GameObject::GameObject(){}

GameObject::GameObject(char sprite, int positionX, int positionY){
  setPositionX(positionX);
  setPositionY(positionY);
  setSprite(sprite);
}
void GameObject::setPositionX(int valor){
  this->positionX += valor;
}

void GameObject::setPositionY(int valor){
  this->positionY += valor;
}

int GameObject::getPositionX(){
  return this->positionX;
}

int GameObject::getPositionY(){
  return this->positionY;
}
void GameObject::setSprite(char sprite){
  this->sprite = sprite;
}
char GameObject::getSprite(){
  return this->sprite;
}
//bool GameObject::Colisoes(char sprite, int positionX, int positionY){

//}
