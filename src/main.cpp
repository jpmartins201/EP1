#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <stdio_ext.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include "Map.hpp"
#include "GameObject.hpp"
#include "Player.hpp"
#include "Trap.hpp"
#include "Bonus.hpp"
#include "Collider.hpp"
#include "Draw.hpp"

using namespace std;
int initMenu(Player * player);
void saveScore(Player * player);


int main()
{
  WINDOW * window;
  char direction, end;
  Map * map1 = new Map();//Instanciando o mapa
  map1->setMap();
  Collider * collider = new Collider();
  Player * player = new Player('w', 2, 2, 4, 0);//Instanciando o player 'w' na posição (2,2)
  Draw * draw = new Draw();
  Bonus * bonus = new Bonus('P', 7, 2, 100);
  Trap * trap = new Trap('@', 4, 4, 1);

  while(initMenu(player) != 1)
  {
    while(player->getLife() >= 0)
    {
      window = initscr();
      start_color();
      init_pair(1, COLOR_MAGENTA, COLOR_WHITE);//Ferramenta para alterar a cor do texto e do fundo da tela
      wbkgd(window, COLOR_PAIR(1));
      clear();
      keypad(stdscr, TRUE);
      noecho();
      map1->addSprite(player->getSprite(), player->getPositionX(), player->getPositionY());
      map1->addSprite(bonus->getSprite(), bonus->getPositionX(), bonus->getPositionY());
      map1->addSprite(trap->getSprite(), trap->getPositionX(), trap->getPositionY());
      draw->DrawMap();

      printw("LIFE: %d \t\t\tScore: %d", player->getLife(), player->getScore());
      direction = getch();
      collider->wallCollider(player, map1, direction);
      collider->bonusCollider(player, bonus, direction);
      collider->trapCollider(player, trap, direction);
      refresh();
      endwin();
    }
    system("CLS");
    cout << "YOU WIN" << endl;
    end = getch();
    if((map1->Detector(player->getPositionX(), player->getPositionY())) == '8')
    {
      printw("YOU WIN!!");
      initMenu(player);
    }
  }

  return 0;
}

void saveScore(Player * player_score)
{
  string playerName;
  int score = player_score->getScore();
  fstream scoreFile;
  system("CLS");
  scoreFile.open("Score.txt");
  cout << "Input your nickname: " << endl;
  getline(cin, playerName);
  scoreFile << "Player: " << playerName << "........." << score << endl;
  scoreFile.close();
}

int initMenu(Player * player){
  //Variables
  char option;
  //Instructions
  cout << "THE LABYRINTH \n \n" << endl;
  cout << "OPTIONS: " << endl;
  cout << "1. PLAY ;" << endl;
  cout << "2. SEE RANKING; " << endl;
  cout << "3. QUIT GAME; " << endl;
  cout << "\n";
  cout << "\n";
  cout << "CONTROLS: WASD BUTTONS - w = UP - a = LEFT - s = DOWN - d = RIGHT \n" << endl;
  cin >> option;
  switch(option)
  {
    case '1':
      return 0;
    case '2':
      saveScore(player);
    case '3':
      return 1;
    default:
      break;
  }
}
